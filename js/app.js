const diyApp = angular.module('diyApp', ['ngRoute', 'ngAnimate', 'firebase']);

diyApp.config(function ($routeProvider) {

  $routeProvider
    .when('/tutoriales', {
      templateUrl: 'views/tutoriales.html',
      controller: 'diyController'
    })
    .when('/club', {
      templateUrl: 'views/club.html',
      controller: 'diyController'
    })
    .when('/articulos', {
      templateUrl: 'views/articulos.html',
      controller: 'diyController'
    })
    .when('/masters', {
      templateUrl: 'views/masters.html',
      controller: 'diyController'
    })
    .when('/tiendas', {
      templateUrl: 'views/tiendas.html',
      controller: 'diyController'
    })
    .otherwise({
      redirectTo: '/tutoriales',
      controller: 'diyController'
    });

  const config = {
    apiKey: "AIzaSyCsB9XQCp-NIJsKFbuOH6P5eKsJOWfz0_E",
    authDomain: "diyclub-firebase.firebaseapp.com",
    databaseURL: "https://diyclub-firebase.firebaseio.com",
    projectId: "diyclub-firebase",
    storageBucket: "diyclub-firebase.appspot.com",
    messagingSenderId: "943141329213"
  };

  firebase.initializeApp(config);

});


diyApp.controller('diyController', ["$scope", "$firebaseObject", "$firebaseArray", "$firebaseAuth", function ($scope, $firebaseObject, $firebaseArray, $firebaseAuth) {

  $scope.openModal = (function () {
    $('.user-modal-box').toggleClass('opened');
    $('#user-btn').toggleClass('active');
  });

  $scope.inOut = (function () {
    $('.signin-btn').toggleClass('in-out');
    $('.create-btn').toggleClass('in-out');
    $('.inout-btn').toggleClass('in-out');
  });

  $scope.auth = $firebaseAuth();

  $scope.signIn = function () {
    $scope.firebaseUser = null;
    $scope.error = null;
    var email = $('#emailTxt').val();
    var pass = $('#passTxt').val();

    $scope.auth.$signInWithEmailAndPassword(email, pass)
      .then(function (firebaseUser) {
        console.log("Signed in as:", firebaseUser.uid);
      }).catch(function (error) {
        console.error("Authentication failed:", error);
      });
  };

  $scope.createUser = function () {
    $scope.firebaseUser = null;
    $scope.error = null;
    var email = $('#emailTxt').val();
    var pass = $('#passTxt').val();

    $scope.auth.$createUserWithEmailAndPassword(email, pass)
      .then(function (firebaseUser) {
        $scope.message = "User created with uid: " + firebaseUser.uid;
      }).catch(function (error) {
        $scope.error = error;
      });
  };

  $scope.auth.$onAuthStateChanged(function(firebaseUser) {
    if (firebaseUser) {
      console.log("Signed in as:", firebaseUser.uid);
      $('.user-img').show();
      $('.user-name').show();
      $('.signin-box').hide();
      $('.ajustes-btn').show();
      $('#signinBtn').hide();
      $('#createBtn').hide();
      $('#logoutBtn').show();
      $('.user-modal-box').removeClass('opened');
    } else {
      console.log("Signed out");
      $('.user-img').hide();
      $('.user-name').hide();
      $('.signin-box').show();
      $('.ajustes-btn').hide();
      $('#signinBtn').show();
      $('#createBtn').show();
      $('#logoutBtn').hide();
      $('.user-modal-box').removeClass('opened');
    }
  });

}]);